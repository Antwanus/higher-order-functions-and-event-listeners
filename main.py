from turtle import Turtle, Screen

t = Turtle()
screen = Screen()

t.pensize(2)


def move_forward_10():
    t.forward(10)
def move_back_10():
    t.back(10)
def turn_left_10():
    t.left(10)
def turn_right_10():
    t.right(10)
def reset_turtle():
    t.clear()
    t.penup()
    t.home()
    t.pendown()


screen.listen()
screen.onkey(key="z", fun=move_forward_10)
screen.onkey(key="s", fun=move_back_10)
screen.onkey(key="q", fun=turn_left_10)
screen.onkey(key="d", fun=turn_right_10)
screen.onkey(key="c", fun=reset_turtle)
screen.exitonclick()
